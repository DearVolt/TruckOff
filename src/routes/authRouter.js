var express = require('express');
var authRouter = express.Router();
var passport = require('passport');
var dataService = require('../services/data-service')();

var router = function() {

    // Route for logging a user in using Passport
    authRouter.route('/login')
        .post(passport.authenticate('local', {
                failureRedirect: '/'
            }), function(req, res) {
                res.redirect('/user/full/1');
            });

    authRouter.route('/register')
        .post(function(req, res) {
            console.log(req.body);
            res.send("Registration request received.");
        });

    return authRouter;
};

module.exports = router;