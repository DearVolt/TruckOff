var express = require('express');
var userRouter = express.Router();
var dataService = require('../services/data-service')();

var router = function() {

    // Route for requesting full profile of user
    userRouter.route('/full/:id')
        .get(function(req, res) {
            var userID = req.params.id;
            console.log("Request for user " + userID);

            dataService.getUser(userID, function(err, userString) {
                if (err) {
                    res.statusCode = 404;
                    res.send("Unable to retrieve user " + userID + ": " + err);
                }
                else {
                    console.log(userString);
                    res.setHeader('Content-Type', 'application/json');
                    res.statusCode = 200;
                    res.send(userString);
                }
            });

            //dataService.end();
        });


    return userRouter;
};

module.exports = router;