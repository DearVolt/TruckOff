// eCYmnG8mGrMfW46w
var mysql = require('mysql');
var connection = mysql.createConnection({
    host: 'localhost',
    user: 'truckuser',
    password: 'eCYmnG8mGrMfW46w',
    database: 'truckoff'
});

var dataService = function() {

    // Returns all info about the user in the database
    var getUser = function(id, done) {
        var sql = "SELECT * FROM Clients WHERE ClientID = ?";
        sql = mysql.format(sql, [id]);
        connection.query(sql, function(err, rows, fields) {
            if (err) {
                console.log("Error querying user: " + err);
                done(1);
            }
            else {
                if (rows.length > 0)
                    done(null, JSON.stringify(rows[0]));
                else
                    done("User does not exist.");
            }
        });
    };

    var addUser = function() {

    };

    var auth = function(user, pass, done) {
        var sql = "SELECT * FROM Clients WHERE CUsername = ? || CEmail = ?";
        sql = mysql.format(sql, [user, user]);
        connection.query(sql, function(err, rows, fields) {
            if (err) {
                console.log("Error querying user: " + err);
                done("Database query failed.");
            }
            else {
                if (rows.length > 0) {
                    if (pass === rows[0].CPassword)
                        done(null, rows[0].ClientID);
                    else
                        done("Error", false, {"message": "The credentials do not match!"});
                }
                else
                    done("Error", false, {"message": "User does not exist."});
            }
        });
    };

    return {
        login: auth,
        getUser: getUser,
        connection: connection
    };
};

module.exports = dataService;