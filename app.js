// Dewald de Jager and Murray le Roux

var express = require('express');
var app = express();
// var bodyParser = require('body-parser');
// var cookieParser = require('cookie-parser');
var passport = require('passport');
// var session = require('express-session');
var dataService = require("./src/services/data-service")();
var port = 4205;
var userRouter = require('./src/routes/userRouter')();
var authRouter = require('./src/routes/authRouter')();

// app.use(express.static('public'));
// app.use(bodyParser.json());
// app.use(bodyParser.urlencoded());
// app.use(cookieParser());
// app.use(session({secret: 'library'}));
// require('./src/config/passport.js')(app);

app.use('/user', userRouter);
app.use('/auth', authRouter);

app.get('/', function(req, res) {
    // res.render('index', {nav: nav});
    res.send("Main");
});

// Run the web server
app.listen(port, function(err) {
    console.log("Running server on port " + port);

    dataService.login("murray", "pokemon", function(err, id) {
       if (err) {
           console.log("Signing in failed: " + err);
       }
        else {
           console.log("Successfully signed in user " + id);
       }
    });
});